package com.example.dianrie.cice_practica_8_multimedia


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.MediaController
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_reproducir.*
import kotlinx.android.synthetic.main.content_reproducir.*

class ReproducirActivity : AppCompatActivity() {

    private val STORAGE_VIDEO_INTENT = 102

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reproducir)
        setSupportActionBar(toolbar)
        title = "REPRODUCIR VIDEO"
        configurarVideoConMediaController(intent.getStringExtra(MainActivity.NAME_EXTRA))

        fab.setOnClickListener { view ->
            // seleccionar un video de la galeria
            Toast.makeText(this, "Selecciona un video de la galeria", Toast.LENGTH_LONG).show()
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "video/mp4"
            startActivityForResult(intent, STORAGE_VIDEO_INTENT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == STORAGE_VIDEO_INTENT) {
            data?.let {
                configurarVideoConMediaController(it.data.toString())
            }
        }

    }

    private var mediaController: MediaController? = null

    fun configurarVideoConMediaController(uri: String) {
        videoView.setVideoPath(uri)
        mediaController = MediaController(this)
        mediaController?.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoView.start()

        videoView.setOnPreparedListener {
            it.isLooping = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
