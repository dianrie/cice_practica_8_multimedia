package com.example.dianrie.cice_practica_8_multimedia

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val NAME_EXTRA = "URI"
    }

    private val CAMARA_VIDEO_INTENT = 101
    private val STORAGE_VIDEO_INTENT = 102
    private val RECORD_REQUEST_CODE = 103
    private var uri: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        title = "GABAR VIDEO"
        //Verificamos que el dispositivo cuenta con camara para poder grabar
        //en el caso de no disponer de camara se oculta el boton de grabacion y se muestra un mensaje
        if (!comprobarCamara()) {
            camaraButton.visibility = View.INVISIBLE
            Toast.makeText(
                this,
                "Este dispositivo no permite grabar ya que no dispone de medios de grabación",
                Toast.LENGTH_LONG
            ).show()
        }
        comprobarPermisos()

        Log.i("TAG", "El dispositivo tiene cámara:${comprobarCamara()} ")
        //Boton flotante que nos envia a otro Activity para reproducir el video grabado
        fab.setOnClickListener { view ->
            if (uri == null) {
                // seleccionar un video de la galeria
                Toast.makeText(this, "Selecciona un video de la galeria", Toast.LENGTH_LONG).show()
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                intent.type = "video/mp4"
                startActivityForResult(intent, STORAGE_VIDEO_INTENT)
            }
        }
    }

    //Iniciamos la camara al apretar el boton grabar
    fun startGrabar(view: View) {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        startActivityForResult(intent, CAMARA_VIDEO_INTENT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMARA_VIDEO_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("TAG", "El video grabado está en: ${data?.data}")
                Toast.makeText(this, "El video grabado está en: ${data?.data}", Toast.LENGTH_LONG).show()
                // uri = data?.data.toString()

                val intent = Intent(this, ReproducirActivity::class.java)
                intent.putExtra(NAME_EXTRA, data?.data.toString())
                startActivity(intent)
            }
        } else if (requestCode == STORAGE_VIDEO_INTENT) {
            data?.let {
                //uri = it.data.toString()
                val intent = Intent(this, ReproducirActivity::class.java)
                intent.putExtra(NAME_EXTRA, it.data.toString())
                startActivity(intent)
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
//-------------------- PERMISOS PARA LA CAMARA ---------------------------

    //Comprueba si el dispisitivo tiene camara
    private fun comprobarCamara(): Boolean {
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }

    private fun comprobarPermisos() {
        val permiso = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (permiso != PackageManager.PERMISSION_GRANTED) {
            Log.i("PERMISO", "Permiso para camara denegado")
            solicitarPermiso()
        } else {
            Log.i("PERMISO", "Permiso para camara permitido")
            Toast.makeText(this, "Permiso para camara permitido", Toast.LENGTH_LONG).show()
        }
    }

    private fun solicitarPermiso() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), RECORD_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RECORD_REQUEST_CODE) {
            if (grantResults.isEmpty() ||
                grantResults[0] != PackageManager.PERMISSION_GRANTED
            ) {
                //No nos ha concedido el permiso
                Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_LONG).show()
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                    //Creamos nuestro propio dialogo
                    val dialog = AlertDialog.Builder(this)
                    dialog.setTitle("IMPORTANTE")
                    dialog.setMessage("Sin el permiso no se podra utilizar la CAMARA")
                    dialog.setPositiveButton("OK") { dialog, which ->
                        Toast.makeText(this, "PERMISO CONCEDIDO", Toast.LENGTH_LONG).show()
                        solicitarPermiso()
                    }
                    val alerta = dialog.create()
                    alerta.show()
                } else {
                    solicitarPermiso()
                }
            } else {
                Toast.makeText(this, "Permiso concedido", Toast.LENGTH_LONG).show()
            }
        }
    }
//-------------------- FIN PERMISOS PARA LA CAMARA ---------------------------
}
